<?php

namespace Scau\Core;

/**
 * 基类
 * @package Scau\Core
 */
class ScauBase
{
    protected $appId = '';
    protected $appSecret = '';
    protected $redirectUri = '';
    protected $accessToken = '';
    public $config = [];

    protected function getOauthDomain(): string
    {
        if ($this->getEnv() === ScauContants::ENV_SANDBOX) {
            return ScauContants::OAUTH_DOMAIN_SANDBOX;
        } else {
            return ScauContants::OAUTH_DOMAIN_PRODUCTION;
        }
    }

    protected function getApiDomain(): string
    {
        if ($this->getEnv() === ScauContants::ENV_SANDBOX) {
            return ScauContants::OAUTH_API_DOMAIN_SANDBOX;
        } else {
            return ScauContants::OAUTH_API_DOMAIN_PRODUCTION;
        }
    }

    protected function getAppId(): string
    {
        return $this->appId;
    }

    protected function getAppSecret(): string
    {
        return $this->appSecret;
    }

    protected function getRedirectUri(): string
    {
        return $this->redirectUri;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    protected function getEnv()
    {
        return $this->getConfig()['env'];
    }

    protected function isDebug()
    {
        return $this->getConfig()['debug'];
    }

    public function getAccessTokenFilePath()
    {
        return $this->getConfig()['access_token_file'];
    }

    public function getDefaultGrant()
    {
        return $this->getConfig()['default_grant'];
    }

    /**
     * 设置环境，此方法为本人测试用，请勿更改env的值
     * @param bool $env true为生产环境，false为测试环境
     */
    public function setEnv(bool $env)
    {
        if ($env) {
            $this->config['env'] = ScauContants::ENV_PRODUCTION;
        } else {
            $this->config['env'] = ScauContants::ENV_SANDBOX;
        }
    }

    /**
     * 获取默认配置
     * @return array
     */
    protected function getDefaultConfig(): array
    {
        return [
            'access_token_file' => __DIR__ . '/access_token.json',
            'env' => ScauContants::ENV_PRODUCTION,
            'debug' => false,
            'default_grant' => ScauContants::OAUTH_DEFAULT_GRANT_ACCOUNT,
            'scopes' => ScauContants::SCOPE_GET_USER_INFO
        ];
    }
}