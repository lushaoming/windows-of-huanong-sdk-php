<?php

namespace Scau\Core;

/**
 * 常量定义类
 * @package Scau\Core
 */
class ScauContants
{
    const ENV_PRODUCTION = 'production';
    const ENV_SANDBOX = 'sandbox';

    const OAUTH_DOMAIN_SANDBOX = 'http://oauth.api.me';
    const OAUTH_DOMAIN_PRODUCTION = 'https://oauth.lushaoming.cn';

    const OAUTH_API_DOMAIN_SANDBOX = 'http://oauth-api.api.me';
    const OAUTH_API_DOMAIN_PRODUCTION = 'https://oauth-api.lushaoming.cn';

    // 默认授权方式
    const OAUTH_DEFAULT_GRANT_ACCOUNT = 'account';
    const OAUTH_DEFAULT_GRANT_WECHAT = 'wechat';

    // 需要的权限
    const SCOPE_GET_USER_INFO = 'getuserinfo';
    const SCOPE_GET_USER_ID = 'getuserid';
    const SCOPE_GET_USER_EMAIL = 'getuseremail';
}