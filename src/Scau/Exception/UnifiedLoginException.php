<?php

namespace Scau\Exception;

/**
 * 统一登录异常类
 * @package Scau\Exception
 */
class UnifiedLoginException extends \Exception
{
}