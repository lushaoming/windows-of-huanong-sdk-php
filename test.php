<?php
// 引入sdk目录的/vendor/autoload.php
require_once __DIR__ . '/vendor/autoload.php';

$config = [
    // access_token文件位置，需要有读写权限
    'access_token_file' => __DIR__ . '/access_token.json'
];

// 注意APPID应该是个字符串，此处的REDIRECT_URI不需要url_encode()
$auth = new \Scau\UnifiedLogin\Authorization('APPID', 'APP SECRET', 'REDIRECT_URI', $config);
// 获取授权的URL，state参数不能超过128字符
$grantPageUrl = $auth->getOauthUrl('test');


/** ------------------- 用户授权后获取用户信息 ----------------------- */
try {
    // 用户授权后，会回调并且携带code参数，通过使用code参数获取用户openid
    $code = $_GET['code'];
    $userInfo = $auth->credential($code);
    print_r($userInfo);
} catch (Exception $e) {// 若获取用户信息失败，会抛出异常
    echo $e->getMessage();
}
