# 华农之窗PHP-SDK授权使用

## 简介
华农之窗统一登录平台是一个提供第三方授权登录的平台，支持账密登录、微信扫码登录，使用Oauth2.0安全授权。
- 申请网站：[华农之窗开放平台](https://open.lushaoming.cn)
- 体验地址：[体验地址](https://oauth.lushaoming.cn/sample/grant)

## 安装方式：
- 使用composer：composer require scauinfo/unified-login
- 下载SDK：[点击下载PHP-SDK](http://forum.lushaoming.cn/wp-content/uploads/2021/02/sdk-php.zip)

## SDK使用

```php
<?php
// 引入sdk目录的/vendor/autoload.php
require_once __DIR__ . '/vendor/autoload.php';

$config = [
    // access_token文件位置，此路径须有读写权限
    'access_token_file' => __DIR__ . '/access_token.json'
];

// 注意APPID应该是个字符串，REDIRECT_URI无需urlencode
$auth = new \Scau\UnifiedLogin\Authorization('APPID', 'APP SECRET', 'REDIRECT_URI', $config);
// 获取授权的URL，state参数不能超过128字符
$grantPageUrl = $auth->getOauthUrl('state');

try {
    // 用户授权后，会回调并且携带code参数，通过使用code参数获取用户openid
    $code = $_GET['code'];
    $userInfo = $auth->credential($code);
    print_r($userInfo);
} catch (Exception $e) {// 若获取用户信息失败，会抛出异常
    echo $e->getMessage();
}
```

[查看SDK使用详情](http://forum.lushaoming.cn/2021/04/29/%e5%8d%8e%e5%86%9c%e4%b9%8b%e7%aa%97php-sdk%e6%8e%88%e6%9d%83%e4%bd%bf%e7%94%a8%e6%95%99%e7%a8%8b/)

其他语言请查看[开发文档](http://forum.lushaoming.cn/2021/02/06/%e5%8d%8e%e5%86%9c%e4%b9%8b%e7%aa%97%e6%8e%88%e6%9d%83%e7%99%bb%e5%bd%95%e5%bc%80%e5%8f%91%e6%96%87%e6%a1%a3/)

如有疑问，请加群：529668978